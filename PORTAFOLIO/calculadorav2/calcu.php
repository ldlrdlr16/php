<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculadora</title>
    <link rel="stylesheet" href="../css.css">
    <script src="calcu.js"></script>
</head>
<body>

    <?php
    //Comprobamos la sesion para mostrar u ocultar elementos del menu
    session_start();
    ?>

    <div class="menu" >
    <a href="../index.php" class="luis">LUIS</a>
        <a href="#" class="letrasMenu finalMenu">Contact</a>
        <a href="../iniciarSesion/registrarse.php" class="letrasMenu">
            <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Registrarse";
                }
            ?>  
        </a>
        <a href="../iniciarSesion/cerrar.php" class="letrasMenu">
            <?php

                if(isset($_SESSION['user_id'])){
                    echo "Salir";
                }
            ?>
        </a>
        <a href="../iniciarSesion/iniciarSesion.php" class="letrasMenu">
        <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Iniciar Sesion";
                }
            ?>
        </a>
        <a href="../index.php" class="letrasMenu">Inicio</a>
    </div>

<br>

    <div class="calcu">

    <br>
    
    <form action="procesar.php" name="form" method="post" autocomplete="off" id="formulario">

        <label for="" class="labels">Primer digito</label>

        <input type="number" id="num1" name="num1" class="configInputs inputt" autofocus onkeypress="return soloNumeros(event)"><br><br>

        <label for="" class="labels">Segundo digito</label>

        <input type="number" id="num2" name="num2" class="configInputs inputt" onkeypress="return soloNumeros(event)"><br><br>

        <input type="submit" name="mas" id="mas" value="+" class="boton botonMargin">
        <input type="submit" name="menos" id="menos" value="-" class="boton">
        <input type="submit" name="multi" id="multi" value="*" class="boton">
        <input type="submit" name="divi" id="divi" value="/" class="boton">
        
    </form>
    <br>

    
    <br><br>

</div>


</body>
</html>
<?php
    //Dando estilo a un echo
    echo '<link rel="stylesheet" href="../css.css">';

    require_once "clases.php";

    //Comprobando que tengan algun valor los inputs
    if (isset($_REQUEST["num1"], $_REQUEST["num2"])) {
        //Obteniendo el valor de los input
        $num1=$_REQUEST["num1"];
        $num2=$_REQUEST["num2"];
        //Variable que contiene una instancia de la clase calculo
        $clase=new calculo();

        //Mostrando el resultado
        echo $clase->calcular($num1,$num2);

    }

    ?>
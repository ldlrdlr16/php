<h1> Practicando... </h1>


<?php

$poderYusme="claustrophobic";

class desembocando {
    public $nivel = 100;
    public $tipo = "rápido";
    
    public function invocarlo(){
        return "el poder maximo de yusme";
    }
}

class canalizar extends desembocando{
    public function fast(string $poderYusme){
        $invocar = new desembocando();
        $cookie = $invocar->invocarlo();

        return "Yusme ha invocado su super poder " . $poderYusme . " el cual es " . $cookie;
    }
}

$mostrar = new canalizar();
echo $mostrar -> fast($poderYusme);

?>
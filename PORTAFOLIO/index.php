<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portafolio</title>
    <link rel="stylesheet" href="css.css">
</head>
<body>
    
    <?php
    //Comprobamos la sesion para mostrar u ocultar elementos del menu
    session_start();
    ?>

    <div class="menu" >
        <a href="index.php" class="luis">LUIS</a>
        <a href="#" class="letrasMenu finalMenu">Contact</a>
        <a href="iniciarSesion/registrarse.php" class="letrasMenu">
            <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Registrarse";
                }
            ?>  
        </a>
        <a href="iniciarSesion/cerrar.php" class="letrasMenu">
            <?php

                if(isset($_SESSION['user_id'])){
                    echo "Salir";
                }
            ?>
        </a>
        <a href="iniciarSesion/iniciarSesion.php" class="letrasMenu">
        <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Iniciar Sesion";
                }
            ?>
        </a>
        
    </div>

    <h1 class="titulo">
        <a href="index.php" class="blanco">Portafolio</a>
    </h1>


    <div class="divs">
        <div class="div1"> <a href="calculadora/index.php" class="margin-auto_content" style="color:white;">Calculadora v1.0</a> </div>
        <div class="div2"> <a href="UI/UI.php" class="margin-auto_content" style="color:black;">UI</a> </div>
        <div class="div1"> <a href="calculadorav2/calcu.php" class="margin-auto_content" style="color:white;">Calculadora v2.0</a> </div>
        <div class="div2"> <a href="UX/UX.PHP" class="margin-auto_content" style="color:black;">UX</a></div>
        <br style="clear: left;" />
    </div>


    <div class="divs">
        <div class="div2"> <a href="formulario/formulario.php " class="margin-auto_content" style="color:black;">Formulario</a> </div>
        <div class="div1"> <a href="protegido/protegido.php" class="margin-auto_content" style="color:white;">Protegido</a> </div>
        <div class="div2"> <a href="#" class="margin-auto_content" style="color:black;"></a> </div>
        <div class="div1"> <a href="posteo/index.php" class="margin-auto_content" style="color:white;">Investigaciones</a></div>
        <br style="clear: left;" />
    </div>


    <div class="divs">
        <div class="div1"> <a href="#" class="margin-auto_content" style="color:white;"></a> </div>
        <div class="div2"> <a href="#" class="margin-auto_content" style="color:black;"></a> </div>
        <div class="div1"> <a href="#" class="margin-auto_content" style="color:white;"></a> </div>
        <div class="div2"> <a href="#" class="margin-auto_content" style="color:black;"></a></div>
        <br style="clear: left;" />
    </div>

    <br>
    
    <p class="pie blanco made">Luis Molina Portafolio 2021</p>

</body>
</html>
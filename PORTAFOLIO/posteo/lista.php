<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Lista</title>
  <link rel="stylesheet" href="../css.css">
</head>
<body>
  
<?php
    //Comprobamos la sesion para mostrar u ocultar elementos del menu
    session_start();
    ?>

    <div class="menu" >
    <a href="../index.php" class="luis">LUIS</a>
        <a href="#" class="letrasMenu finalMenu">Contact</a>
        <a href="../iniciarSesion/registrarse.php" class="letrasMenu">
            <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Registrarse";
                }
            ?>  
        </a>
        <a href="../iniciarSesion/cerrar.php" class="letrasMenu">
            <?php

                if(isset($_SESSION['user_id'])){
                    echo "Salir";
                }
            ?>
        </a>
        <a href="../iniciarSesion/iniciarSesion.php" class="letrasMenu">
        <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Iniciar Sesion";
                }
            ?>
        </a>
        <a href="../index.php" class="letrasMenu">Inicio</a>
    </div>

<br>


<div class="registrar">
<a href="registrar.php" class="blanco verdana">Registrar</a>
</div>

<br>

<div class="formulario">
  <h1 class="blanco">Lista de Investigaciones</h1>
  <ul>
    <?php foreach ($posts as $post): ?>
      <li>
        <h2 class="blanco">
          <a href="mostrar.php?id=<?php echo $post['id'] ?>" class="blanco"> <?php echo $post['titulo'] ?> </a>

          <a href="<?= 'borrar.php?id=' . $post['id'] ?>"class="blanco letrasMenu">🗑️Borrar</a>
          <a href="<?= 'editar.php?id=' . $post['id'] ?>"class="blanco letrasMenu">✏️Editar</a>

        </h2>
      </li>
    <?php endforeach; ?>
  </ul>

</div>






</body>
</html>
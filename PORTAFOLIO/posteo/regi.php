<?php

require_once 'clases.php';
//variable con la instancia de la clase post
$clase = new post();

//Comprobamos si hubo un envio del formulario
if (isset($_POST['registrar'])) {
    //Guardamos el valor de los input en las variables php
    $autor = $_POST['autor'];
    $titulo = $_POST['titulo'];
    $contenido = $_POST['contenido'];

    //Instanciamos el metodo de la clase post y le pasamos los parametros
    $clase->registrarlo($autor,$titulo,$contenido);
                
}

?>
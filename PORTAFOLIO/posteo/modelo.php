<?php



class conexion{
    //metodo para establecer la conexion a la base de datos
    public function conexion(){
        require_once 'baseDeDatos.php'; 
         $conex=new mysqli(HOST, USER, PASSWORD, DATABASE); 
          
            return $conex;
        }    
}

class poste{
    //Aqui ejecutamos una consulta sql y la retornamos
    function posteos(){ 
        $clase=new conexion();
        $mysqli = $clase->conexion();
        
        $resultado = $mysqli->query('SELECT id, titulo FROM post ORDER BY id desc'); 
        
        return $resultado; 
        }
}

 
class post{
    //Ejecuta una consulta sql con el parametro id
    function postId($id)
    {

    $clase=new conexion();
    $mysqli = $clase->conexion();
    
    $resultado = $mysqli->query('SELECT fecha, titulo, contenido, autor FROM post WHERE id ='.$id);

    //El resultado se convierte en una matriz asociativa para tratarla como una fila
    $fila = mysqli_fetch_assoc($resultado);
    
        //retornamos la fila
        return $fila;
    }
}


class edit{
    //Ejecuta una sentencia sql
    //Posee un parametro
    public function editar($id)
    {
        require 'baseDeDatos.php';

        $query = $conexion->prepare("SELECT * FROM post WHERE id=:id");

        $query->bindParam("id", $id, PDO::PARAM_STR);
 
        $query->execute();

        $resultado = $query->fetch(PDO::FETCH_ASSOC);

        if (!$resultado) {
            echo '<p class="error">La pagina no existe</p>';
            header('refresh:5;url=index.php');
        }

        return $resultado;


    }

}

class actualiza{

    //Ejecuta la actualizacion de los datos
    //Posee 4 parametros
    public function actualizar($id,$autor,$titulo,$contenido){
        require 'baseDeDatos.php';

        $query = $conexion->prepare("UPDATE post SET autor=:autor, titulo=:titulo, contenido=:contenido WHERE id=:id");

        $query->bindParam("id", $id, PDO::PARAM_STR);

        $query->bindParam("autor", $autor, PDO::PARAM_STR);

        $query->bindParam("titulo", $titulo, PDO::PARAM_STR);

        $query->bindParam("contenido", $contenido, PDO::PARAM_STR);

        $query->execute();

        $resultado = $query->fetch(PDO::FETCH_ASSOC);

        

        if (!$resultado) {
            echo '<p class="correcto">Datos actualizados correctamente! </p>';
            header('refresh:5;url=index.php');
        }
        else{
            echo '<p class="error">Algo salió mal</p>';
            header('refresh:5;url=index.php');
        }

    }
}

class borra{

    //Borra una registro de la base de datos
    //Posee un parametro
    public function borrar($id){

        require 'baseDeDatos.php';
        
        $query = $conexion->prepare("DELETE FROM post WHERE id =:id");

        $query->bindParam("id", $id, PDO::PARAM_STR);

        $query->execute();

        $resultado = $query->fetch(PDO::FETCH_ASSOC);

        if (!$resultado) {
            echo '<p class="correcto">Eliminado correctamente!</p>';
            header('refresh:5;url=index.php');
        }
        else{
            echo '<p class="error">Algo salió mal</p>';
            header('refresh:5;url=index.php');
        }
    }
}
 
?>
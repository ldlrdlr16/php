document.addEventListener("DOMContentLoaded", function() {
  document.getElementById("registrar").addEventListener("submit", validar); 
});


function validar(e) {

//Variables que contienen el valor de los inputs
let autor = document.getElementById('autor').value;
let titulo = document.getElementById('titulo').value;
let contenido = document.getElementById('contenido').value;



if(autor.length < 1) {
  e.preventDefault();
  alert('Debe escribir algo en autor');
  document.getElementById("autor").focus();
  return;
}
if (titulo.length < 1) {
  e.preventDefault();
  alert('Debe escribir algo en titulo');
  document.getElementById("titulo").focus();
  return;
}
if (contenido.length < 1) {
  e.preventDefault();
  alert('Debe escribir algo en contenido');
  document.getElementById("contenido").focus();
  return;
}



this.submit();
}


//Funcion para permitir al usuario ingresar solo números, letras y espacios.
function noSimbolos(e){
  let unicode=e.keyCode? e.keyCode : e.charCode;

  if(unicode==8 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40)
    return true;


  patron= /[A-Za-z0-9 ]/;
  tecla_final = String.fromCharCode(unicode);
  return patron.test(tecla_final);
}

//Function para permitir al usuario ingresar solo números, letras, espacios y puntos.
function noSimbolosContenido(e){
  let unicode=e.keyCode? e.keyCode : e.charCode;

  if(unicode==8 || unicode==46 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40)
    return true;


  patron= /[A-Za-z0-9, ]/;
  tecla_final = String.fromCharCode(unicode);
  return patron.test(tecla_final);
}



//Lo mismo que noSimbolos pero sin espacio
function noSimbolosClave(e){
  let unicode=e.keyCode? e.keyCode : e.charCode;

  if(unicode==8 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40)
    return true;


  patron= /[A-Za-z0-9]/;
  tecla_final = String.fromCharCode(unicode);
  return patron.test(tecla_final);
}


//Function para limitar el ingreso de caracteres en un input
function limitar(e,valor,caracteres) {

  let unicode=e.keyCode? e.keyCode : e.charCode;

  // Permitimos las siguientes teclas:
  // 8 backspace
  // 46 suprimir
  // 13 enter
  // 9 tabulador
  // 37 izquierda
  // 39 derecha
  // 38 subir
  // 40 bajar
  //36 46
   if(unicode==8 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40)
    return true;

  // Si ha superado el limite de caracteres devolvemos false
  if (valor.length >= caracteres) {
    return false;
  }

  return true;
  
}


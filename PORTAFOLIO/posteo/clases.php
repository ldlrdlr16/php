<?php
//esta clase contiene el metodo registrarlo
class post{
    //Metodo para insertar en la base de datos una investigacion, recibe 3 parametros
    public function registrarlo($autor,$titulo,$contenido){
        require "../iniciarSesion/baseDeDatos.php";

        

            //script que contiene la consulta sql
            $query = $conexion->prepare("INSERT INTO post(autor,titulo,contenido) VALUES (:autor,:titulo,:contenido)");
            //El bindParam se usa como puntero para establecer una referencia
            // de los valores insertados a las variables php

            $query->bindParam("autor", $autor, PDO::PARAM_STR);
            $query->bindParam("titulo", $titulo, PDO::PARAM_STR);
            $query->bindParam("contenido", $contenido, PDO::PARAM_STR);
            
            //Ejecutando el query y guardando en una variable booleana el valor
            $resultado = $query->execute();
            
            //Comprobando si es true o false para mostrar el debido mensaje
            if ($resultado) {
                echo '<p class="correcto">Su registro fue exitoso!</p>';
                header('refresh:3;url=index.php');
            } 
            else {
                echo '<p class="error">Algo salió mal!</p>';
                header('refresh:5;url=registrar.php');
            }

    }

}
?>

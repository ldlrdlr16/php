<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrar</title>
    <link rel="stylesheet" href="../css.css">
    <script src="val_regi.js"></script>
</head>
<body>
    
<?php
    //Comprobamos la sesion para mostrar u ocultar elementos del menu
    session_start();
    ?>

    <div class="menu" >
    <a href="../index.php" class="luis">LUIS</a>
        <a href="#" class="letrasMenu finalMenu">Contact</a>
        <a href="../iniciarSesion/registrarse.php" class="letrasMenu">
            <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Registrarse";
                }
            ?>  
        </a>
        <a href="../iniciarSesion/cerrar.php" class="letrasMenu">
            <?php

                if(isset($_SESSION['user_id'])){
                    echo "Salir";
                }
            ?>
        </a>
        <a href="../iniciarSesion/iniciarSesion.php" class="letrasMenu">
        <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Iniciar Sesion";
                }
            ?>
        </a>
        <a href="../index.php" class="letrasMenu">Inicio</a>
    </div>

    <br>

    <div class="registrar">
        <a href="index.php" class="blanco verdana">Ver Lista</a>
    </div>



    <br>


    <form method="post" action="regi.php" id="registrar" class="formulario">

            <h1>Registrar</h1>
            
           

            <label>Autor</label>
            <input type="text" id="autor" name="autor" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autofocus autocomplete="off"/>

 
            <label>Titulo</label>
            <input type="text" id="titulo" name="titulo" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,20)" onkeydown="return limitar(event,this.value,20)" class="inputt" autocomplete="off"/>


            <label>Contenido</label>
            <textarea  id="contenido" class="verdana" name="contenido" cols="80" rows="28" onkeypress="return noSimbolosContenido(event)" onkeyup="return limitar(event,this.value,1300)" onkeydown="return limitar(event,this.value,1300)" class="inputt" autocomplete="off"></textarea>

            <br>
            <button type="submit" name="registrar" value="registrar">Confirmar</button>
            <br>
    </form>
  

</body>
</html>
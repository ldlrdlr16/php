<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Mostrar</title>
  <link rel="stylesheet" href="../css.css">
</head>
<body>

    <?php
    //Comprobamos la sesion para mostrar u ocultar elementos del menu
    session_start();
    ?>

    <div class="menu" >
    <a href="../index.php" class="luis">LUIS</a>
        <a href="#" class="letrasMenu finalMenu">Contact</a>
        <a href="../iniciarSesion/registrarse.php" class="letrasMenu">
            <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Registrarse";
                }
            ?>  
        </a>
        <a href="../iniciarSesion/cerrar.php" class="letrasMenu">
            <?php

                if(isset($_SESSION['user_id'])){
                    echo "Salir";
                }
            ?>
        </a>
        <a href="../iniciarSesion/iniciarSesion.php" class="letrasMenu">
        <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Iniciar Sesion";
                }
            ?>
        </a>
        <a href="../index.php" class="letrasMenu">Inicio</a>
    </div>

  <br>

  <?php require_once 'modelo.php'; ?>
  
  <?php 
  $clase=new post();
  $post = $clase->postId($_GET['id']); ?>

  <div class="registrar">
    <a href="index.php" class="blanco verdana">Ver Lista</a>
  </div>

  

<br>

<div class="mostrar">

  <br>

  <h1 class="hone"><?php echo $post['titulo'] ?></h1>

  <div class="blanco margin">
  <span>Fecha de publicación:</span>
  <?php echo $post['fecha'] ?>

  </div>

  <br>

  <div class="blanco margin" >
  <span>Autor:</span>
  <?php echo $post['autor'] ?>
  
  </div>

  <div class="p blanco">

    <p><?php echo $post['contenido'] ?></p>
    <br><br><br>

  </div>

  <br><br>

</div>

</body>
</html>
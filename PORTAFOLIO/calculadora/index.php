<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculadora</title>
    <link rel="stylesheet" href="../css.css">
    <script src="calcular.js"></script>
</head>
<body>

    <?php
    //Comprobamos la sesion para mostrar u ocultar elementos del menu
    session_start();
    ?>

    <div class="menu" >
    <a href="../index.php" class="luis">LUIS</a>
        <a href="#" class="letrasMenu finalMenu">Contact</a>
        <a href="../iniciarSesion/registrarse.php" class="letrasMenu">
            <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Registrarse";
                }
            ?>  
        </a>
        <a href="../iniciarSesion/cerrar.php" class="letrasMenu">
            <?php

                if(isset($_SESSION['user_id'])){
                    echo "Salir";
                }
            ?>
        </a>
        <a href="../iniciarSesion/iniciarSesion.php" class="letrasMenu">
        <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Iniciar Sesion";
                }
            ?>
        </a>
        <a href="../index.php" class="letrasMenu">Inicio</a>
    </div>

    <br>

    <form action="procesar.php" method="post" id="formulario">

        <label for="" class="blanco">Valor 1</label>

        <br>
        <br>

        <input type="number" id="val1" name="val1" autocomplete="off" autofocus onkeypress="return soloNumeros(event)">

        <br>
        <br>

        <label for="" class="blanco">Valor 2</label>

        <br>
        <br>

        <input type="number" id="val2" name="val2" autocomplete="off" onkeypress="return soloNumeros(event)">

        <br>
        <br>

        <select name="opcion" id="">
            <option value="Sumar">Sumar</option>
            <option value="Restar">Restar</option>
            <option value="Multiplicar">Multiplicar</option>
            <option value="Dividir">Dividir</option>
        </select>

        <br>
        <br>

        <button type="submit">Calcular</button>
        
    </form>
</body>
</html>
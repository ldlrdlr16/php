<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
    <link rel="stylesheet" href="../css.css">
    <script src="val_regi.js"></script>
</head>
<body>
    
    <?php
    //Comprobamos la sesion para mostrar u ocultar elementos del menu
    session_start();
    ?>

    <div class="menu" >
    <a href="../index.php" class="luis">LUIS</a>
        <a href="#" class="letrasMenu finalMenu">Contact</a>
        <a href="registrarse.php" class="letrasMenu">
            <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Registrarse";
                }
            ?>  
        </a>
        <a href="cerrar.php" class="letrasMenu">
            <?php

                if(isset($_SESSION['user_id'])){
                    echo "Salir";
                }
            ?>
        </a>
        <a href="iniciarSesion.php" class="letrasMenu">
        <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Iniciar Sesion";
                }
            ?>
        </a>
        <a href="../index.php" class="letrasMenu">Inicio</a>
    </div>

    <br>


    <form method="post" action="regi.php" id="regi" class="formulario">

            <h1>Registrarse</h1>
            
            <br>

            <label>Usuario</label>
            <input type="text"     id="nombreUsuario" name="nombreUsuario" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,20)" onkeydown="return limitar(event,this.value,20)" class="inputt" autofocus autocomplete="off"/>

 
            <label>Correo</label>
            <input type="email"    id="correo"        name="correo"        onkeyup="return limitar(event,this.value,38)" onkeydown="return limitar(event,this.value,38)" class="inputt"  autocomplete="off"/>


            <label>Contraseña</label>
            <input type="password" id="clave"         name="clave"         onkeypress="return noSimbolosClave(event)" onkeyup="return limitar(event,this.value,10)" onkeydown="return limitar(event,this.value,10)" class="inputt"  autocomplete="off"/>

            <label>Verificar Contraseña</label>
            <input type="password" id="clave2"        name="clave2"        onkeypress="return noSimbolosClave(event)" onkeyup="return limitar(event,this.value,10)" onkeydown="return limitar(event,this.value,10)" class="inputt"  autocomplete="off"/>

            <br>

            <button type="submit" name="registrarse" value="registrarse">Confirmar</button>





            <br>
    </form>

</body>
</html>
<?php

require_once "clases.php";

//variable que contiene la instancia de la clase registrar
$clase = new registrar();

//comprobamos si se envio el formulario
if (isset($_POST['registrarse'])) {

    //almacenamos el valor de los inputs en las variables php
    $nombreUsuario = $_POST['nombreUsuario'];
    $correo = $_POST['correo'];
    $clave = $_POST['clave'];

    //instanciamos el metodo de la clase registrar
    $clase->registrarse($nombreUsuario,$correo,$clave);
                
}


?>
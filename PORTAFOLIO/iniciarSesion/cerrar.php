<?php

    //Este script es para finalizar la sesion
    //Ejecutamos la funcion sesion_start para comprobar la sesion activa
    session_start();
    //Ejecutamos la funcion session_destroy para cerrarla
    session_destroy();
    //Enviamos al usuario a iniciar sesion
    header('location: iniciarSesion.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio de sesión</title>
    <link rel="stylesheet" href="../css.css">
    <script src="val_ini.js"></script>
</head>
<body>

    <?php
    //Comprobamos la sesion para mostrar u ocultar elementos del menu
    session_start();
    ?>

    <div class="menu" >
    <a href="../index.php" class="luis">LUIS</a>
        <a href="#" class="letrasMenu finalMenu">Contact</a>
        <a href="registrarse.php" class="letrasMenu">
            <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Registrarse";
                }
            ?>  
        </a>
        <a href="cerrar.php" class="letrasMenu">
            <?php

                if(isset($_SESSION['user_id'])){
                    echo "Salir";
                }
            ?>
        </a>
        <a href="iniciarSesion.php" class="letrasMenu">
        <?php

                if(!isset($_SESSION['user_id'])){
                    echo "Iniciar Sesion";
                }
            ?>
        </a>
        <a href="../index.php" class="letrasMenu">Inicio</a>
    </div>

    <br>

    <form method="post" action="ini.php" id="ini" class="formulario">

        <h1>Iniciar Sesión</h1>
            
        <br>

        <label>Usuario</label>
        <input  type="text"     id="nombre" name="nombreUsuario" class="inputt" onkeypress="return noSimbolos(event)"      onkeyup="return limitar(event,this.value,20)" onkeydown="return limitar(event,this.value,20)" autocomplete="off" autofocus />

        <label>Contraseña</label>
        <input  type="password" id="clave"  name="clave"         class="inputt" onkeypress="return noSimbolosClave(event)" onkeyup="return limitar(event,this.value,10)" onkeydown="return limitar(event,this.value,10)" autocomplete="off"/>

        <br>
 
        <button type="submit"  id="enviar" name="iniciarSesion" value="iniciarSesion">Confirmar</button>



        <br>
    </form>

</body>
</html>
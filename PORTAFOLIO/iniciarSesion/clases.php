<?php

//clase que permite iniciar sesion
class iniciarSesion{
    //metodo de la clase
    //posee 2 parametros
    public function iniciar($nombreUsuario,$clave){
            require "baseDeDatos.php";
            
            //prepara la sentencia sql que sera ejecutada por el metodo execute
            $query = $conexion->prepare("SELECT * FROM usuarios WHERE nombreUsuario=:nombreUsuario");
            //hacemos referencia del valor de las variables al valor de la sentencia
            $query->bindParam("nombreUsuario", $nombreUsuario, PDO::PARAM_STR);
            //ejecutamos la sentencia sql
            $query->execute();

            //obtenemos la fila devuelta por la base de datos
            $resultado = $query->fetch(PDO::FETCH_ASSOC);

            //Si es false mostramos error
            if (!$resultado) {
                echo '<p class="error">El usuario o la contraseña es incorrecto</p>';
                header('refresh:5;url=iniciarSesion.php');
            } 
            //Sino, verificamos la contraseña
            else {
                //Si la contraseña es correcta, establecemos los datos del usuario en la matriz de sesion
                if (password_verify($clave, $resultado['clave'])) {
                    $_SESSION['user_id'] = $resultado['id'];
                    $_SESSION['user_name'] = $nombreUsuario;
                    //le mostramos al usuario un mensaje avisando que su inicio de sesion fue exitoso
                    echo '<p class="correcto">Inicio de sesion exitoso!</p>';
                    //lo enviamos a la pagina que le da la bienvenida
                    header('refresh:3;url=../protegido/protegido.php');
                    exit;
                } 
                //Si la contraseña es incorrecta, le mostramos el mensaje de error y devuelta a iniciar sesion
                else {
                    echo '<p class="error">El usuario o la contraseña es incorrecto</p>';
                    header('refresh:5;url=iniciarSesion.php');
                }
            }
        


    }
}

//clase que permite registrarse
class registrar{
    //el metodo 
    //contiene 3 parametros
    public function registrarse($nombreUsuario,$correo,$clave){
                require "baseDeDatos.php";

                
                //encriptando la clave por medio del algoritmo hash
                $clave_hash = password_hash($clave, PASSWORD_BCRYPT);

                //preparamos la sentencia que sera ejecutada
                $query = $conexion->prepare("SELECT * FROM usuarios WHERE correo=:correo");
                //vinculamos la variable php a un parametro correspondiente a 
                // la sentencia sql que fue preparada
                $query->bindParam("correo", $correo, PDO::PARAM_STR);
                //ejecutamos la sentencia
                $query->execute();

                //Si la sentencia devuelve una fila o mas, mostramos en mensaje de error
                if ($query->rowCount() > 0) {
                    echo '<p class="error">La dirección de correo electrónico que ha ingresado ya está registrada!</p>';
                    header('refresh:5;url=registrarse.php');
                }

                //Si la sentencia no devuelve filas, procedemos a registrar

                if ($query->rowCount() == 0) {
                    //Hacemos referencia del valor de las variables al valor de la sentencia
                    $query = $conexion->prepare("INSERT INTO usuarios(nombreUsuario,clave,correo) VALUES (:nombreUsuario,:clave_hash,:correo)");
                    $query->bindParam("nombreUsuario", $nombreUsuario, PDO::PARAM_STR);
                    $query->bindParam("clave_hash", $clave_hash, PDO::PARAM_STR);
                    $query->bindParam("correo", $correo, PDO::PARAM_STR);
                    
                    //Ejecutamos la sentencia sql
                    $resultado = $query->execute();

                    //Si la sentencia devuelve true mostramos un mensaje de registro exitoso
                    if ($resultado) {
                        echo '<p class="correcto">Su registro fue exitoso!</p>';
                        //enviamos al usuario a iniciar sesion
                        header('refresh:3;url=iniciarSesion.php');
                    } 
                    //Si no, mostramos el mensaje de error
                    else {
                        echo '<p class="error">El usuario ya esta registrado!</p>';
                        
                        //enviamos al usuario a registrarse
                        header('refresh:5;url=registrarse.php');
                    }
                }

    }
}



?>

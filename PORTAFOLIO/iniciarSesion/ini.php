<?php
session_start();

require_once "clases.php";
//variable que contiene la instancia de la clase iniciarSesion
$clase = new iniciarSesion();

//comprobamos si se envio el formulario
if (isset($_POST['iniciarSesion'])) {
    //almacenamos el valor de los input en las variables php
    $nombreUsuario = $_POST['nombreUsuario'];
    $clave = $_POST['clave'];

    //instanciamos el metodo de la clase iniciarSesion
    $clase->iniciar($nombreUsuario,$clave);


}

?>
/*Escribir este codigo en el gestor mysql hara que se cree toda la estructura necesaria para esta aplicacion*/
CREATE DATABASE php;

use php;

CREATE TABLE `php`.`usuarios` 
( `id` INT NOT NULL AUTO_INCREMENT , 
`nombreUsuario` VARCHAR(25) NOT NULL , 
`clave` VARCHAR(255) NOT NULL , 
`correo` VARCHAR(100) NOT NULL , 
PRIMARY KEY (`id`)) ENGINE = InnoDB;


CREATE TABLE `php`.`post` 
( `id` INT NOT NULL AUTO_INCREMENT , 
`autor` VARCHAR(30) NOT NULL , 
`titulo` VARCHAR(50) NOT NULL , 
`contenido` VARCHAR(5000) NOT NULL , 
`fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , 
PRIMARY KEY (`id`)) ENGINE = InnoDB;
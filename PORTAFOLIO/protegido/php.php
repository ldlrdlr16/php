<?php


require_once "clases.php";
//Obteniendo el nombre del usuario con la sesion activa
$nombre = $_SESSION['user_name'];
//Instancia de la clase mostrar
$codigo = new mostrar();
//Si devuelve false la funcion isset para el nombre del usuario entonces lo enviamos a iniciar sesion
if(!isset($_SESSION['user_id'])){
    header('Location: ../iniciarSesion/iniciarSesion.php');
    exit;
} 
//Y si no
else {
    // Mostramos al usuario la pagina
    echo $codigo->mostrarse($nombre);
    
}



?>